import pymysql.cursors
class dataBase:
    def __init__(self):
        __session = None
        __connection = None

        try:
            db = pymysql.connect(
                    host='localhost',
                    port=3306,
                    user='root',
                    passwd='password',
                    db='Stackoverflow')
            self.__connection = db
            self.__session = db.cursor()

        except Exception as e:
            print("Não foi possivel conectar com banco")
            raise e

    def getBody(self,idRange, step):
        numbers = (idRange,idRange + step)
        sql = 'SELECT body FROM posts WHERE Id_aux >= %d && Id_aux < %d'% numbers
        self.__session.execute(sql)
        bodyList = []

        for body in self.__session.fetchall():
            bodyList.append(body)

        return bodyList

    def select (self):
        self.__session.execute("SELECT body FROM posts WHERE Id_aux = 1")
        return self.__session.fetchall()
