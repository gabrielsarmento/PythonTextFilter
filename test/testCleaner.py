#!env/bin/pytest
import sys
sys.path.append('/home/gabriel/Documentos/PythonTextFilter')
import unittest
from classes.ClassCleaner import TextCleaner

class TestCleaner(unittest.TestCase):
    def setUp(self):
        pass

    def tearDown(self):
        pass

    def test_cleaer(self):
        text = "<p>I want to format my existing comments as 'RDoc comments' so they can be viewed using <code>ri</code>.</p>&#xA;&#xA;<p>What are some recommended resources for starting out using RDoc?</p>&#xA;"

        output ="I want to format my existing comments as 'RDoc comments' so they can be viewed using. What are some recommended resources for starting out using RDoc?"

        cleaner = TextCleaner()
        self.assertEquals(cleaner.filter(text),output)
