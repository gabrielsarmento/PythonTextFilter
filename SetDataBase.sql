
create database Stackoverflow DEFAULT CHARACTER SET utf8 ;

use Stackoverflow;

CREATE TABLE posts (
    Id_aux INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    Id INT NOT NULL,
    PostTypeId SMALLINT,
    AcceptedAnswerId INT,
    ParentId INT,
    Score INT NULL,
    ViewCount INT NULL,
    Body text NULL,
    OwnerUserId INT NOT NULL,
    LastEditorUserId INT,
    LastEditDate DATETIME,
    LastActivityDate DATETIME,
    Title varchar(256) NOT NULL,
    Tags VARCHAR(256),
    AnswerCount INT NOT NULL DEFAULT 0,
    CommentCount INT NOT NULL DEFAULT 0,
    FavoriteCount INT NOT NULL DEFAULT 0,
    CreationDate DATETIME
);



load XML local infile '/home/gabriel/Documentos/Repositorios/PythonTextFilter/PostsHead.xml'
into table posts
rows identified by '<row>';
