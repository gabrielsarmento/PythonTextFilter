from classes.ClassDB import dataBase as db
from classes.ClassCleaner import TextCleaner

dataBase = db()
cleaner = TextCleaner()
idRange = 1
step = 2

while(True):
    bodyList = dataBase.getBody(idRange,step)
    idRange += step

    for post in bodyList:
        print(cleaner.filter(post),'\n\n')
        
    if  not bodyList:
        break
