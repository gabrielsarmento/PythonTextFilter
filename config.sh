virtualenv -p python3 env
. env/bin/activate

pip install -r requirements.txt
chmod +x docs/git-hooks/*
cp docs/git-hooks/* .git/hooks
